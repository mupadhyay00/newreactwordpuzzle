export const CREATEOPTIONS = 'CREATEOPTIONS';
export const FILLPUZZLE = 'FILLPUZZLE';
export const CLEANSELECTMOVES = 'CLEANSELECTMOVES';
export const CLEANSELECTWORDS = 'CLEANSELECTWORDS';
export const CLEANPUZZLE = 'CLEANPUZZLE';
export const GETALLSELECTEDWORDS = 'GETALLSELECTEDWORDS';
export const GETALLSELECTEDMOVES = 'GETALLSELECTEDMOVES';
export const MOVESSELECTED = 'MOVESSELECTED';

const user1 = {
  name: 'Mritunjay',
  email: 'mupadhyay@gmail.com',
  points: 0,
  color: '#00ff00'
};
const user2 = {
  name: 'Manji',
  email: 'mritunjayupdhyay@gmail.com',
  points: 0,
  color: '0000ff'
};

export const endgame = () => {
  return { type: 'endgame' };
};

export const addUser = (obj) => {
  const response = firebase.database().ref('player/').set(obj);
  return (dispatch) => {
    response.then(() => {
      dispatch({ type: 'addUser', payload: obj });
    });
  };
};

export const removeUser = () => {
  const response = firebase.database().ref('player/').set(null);
  return (dispatch) => {
    response.then(() => {
      dispatch({ type: 'removeUser' });
    });
  };
};

export const cleanPuzzle = () => {
  const response = firebase.database().ref('puzzle').set(null);
  return (dispatch) => {
    response.then(() => {
      dispatch({ type: CLEANPUZZLE });
    });
  };
};

export const createOptions = (opts, wordList) => {
  const options = {};
  options.width = opts.width || wordList[0].length;
  options.height = opts.height || wordList[0].length;
  options.preferOverlap = opts.preferOverlap === undefined ? true : opts.preferOverlap;
  options.fillBlanks = opts.fillBlanks === undefined ? true : opts.fillBlanks;
  options.maxattempts = opts.maxattempts || 3;
  return {
    type: CREATEOPTIONS,
    payload: options
  };
};

export const fillPuzzle = (puzzle) => {
  const response = firebase.database().ref('puzzle').set(puzzle);
  return (dispatch) => {
  response.then(() => {
    dispatch({ type: FILLPUZZLE, payload: puzzle });
  });
};
};
export const cleanSelectedMoves = () => {
  const response = firebase.database().ref('selectedMoves').set(null);
  return (dispatch) => {
    response.then(() => {
      dispatch({ type: CLEANSELECTMOVES });
    });
  };
};
export const cleanSelectedWords = () => {
  const response = firebase.database().ref('selectedWords').set(null);
  return (dispatch) => {
    response.then(() => {
      dispatch({ type: CLEANSELECTWORDS });
    });
  };
};
export const getWords = () => {
  const objLocal = { words: null };
  const response = firebase.database().ref('words').once('value', (snapshot) => {
    objLocal.words = snapshot.val();
  });
  return (dispatch) => {
    response.then(() => {
      dispatch({ type: 'GETWORDS', payload: objLocal.words });
    });
  };
};

export const movesSelected = (arr, user, id, word) => {
  const newArr = arr.map((d) => {
    const p = {};
    p.x = d.dataset.x;
    p.y = d.dataset.y;
    return p;
  });
  const selectedmove = {};
  selectedmove.move = newArr;
  selectedmove.player = user.email;
  selectedmove.word = word;
  const response = firebase.database().ref(`selectedMoves/${id}`).set(selectedmove);
  return (dispatch) => {
    response.then(() => {
      dispatch({ type: MOVESSELECTED });
    });
  };
};

export const wordSelected = (word, user, id) => {
  const selectedword = {};
  selectedword.word = word;
  selectedword.player = user.name;
  const response = firebase.database().ref(`selectedWords/${id}`).set(selectedword);
  return (dispatch) => {
    response.then(() => {
      dispatch({ type: 'wordSelected' });
    });
  };
};

export const getSelectedMoves = (moves) => {
  return { type: GETALLSELECTEDMOVES, payload: moves };
};

export const getSelectedWords = (words) => {
    return { type: GETALLSELECTEDWORDS, payload: words };
};

export const addPlayers = (id) => {
  const users = [];
  users.push(user1);
  users.push(user2);
  const response = firebase.database().ref('users').set(users);
      return (dispatch) => {
        response.then(() => {
          dispatch({ type: 'ADDPLAYERS', payload: id });
        });
      };
};

export const addPoints = (user, id) => {
  const response = firebase.database().ref(`users/${id}/points`).set(user.points + 3);
  return (dispatch) => {
    response.then(() => {
      dispatch({ type: 'addPoints' });
    });
  };
};


export const getPlayers = (id) => {
  const objLocal = { players: null, id };
  const response = firebase.database().ref('users').once('value', (snapshot) => {
    objLocal.players = snapshot.val();
  });
  return (dispatch) => {
    response.then(() => {
      dispatch({ type: 'GETPLAYERS', payload: objLocal });
    });
  };
};
