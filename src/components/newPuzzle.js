import React, { Component } from 'react';
import { connect } from 'react-redux';
import Words from './words.js';
import { createOptions, fillPuzzle, addPlayers, getPlayers,
   changeCurrentUser, addPoints, wordSelected, getWords,
    getSelectedWords, cleanSelectedWords, cleanSelectedMoves,
     getSelectedMoves, movesSelected, endgame, cleanPuzzle } from '../actions/index.js';

class NewPuzzle extends Component {
  constructor(props) {
        super(props);
        this.myobj = {
          startSquare: null,
          selectedSquares: [],
          curWord: '',
          curOrientation: null,
          puzzleCons: ''
        };
        this.fillPuzzle = this.fillPuzzle.bind(this);
        this.findBestLocations = this.findBestLocations.bind(this);
        this.pruneLocations = this.pruneLocations.bind(this);
        this.placeWord = this.placeWord.bind(this);
        this.placeWordInPuzzle = this.placeWordInPuzzle.bind(this);
        this.fillBlanks = this.fillBlanks.bind(this);
        this.printPuzzle = this.printPuzzle.bind(this);
        this.getWords = this.getWords.bind(this);
        this.callFillPuzzle = this.callFillPuzzle.bind(this);
        this.getPlayers = this.getPlayers.bind(this);
        this.toUpdateData = this.toUpdateData.bind(this);
        this.GameOrScore = this.GameOrScore.bind(this);
        this.scoreOfAll = this.scoreOfAll.bind(this);
        this.getUser = this.getUser.bind(this);
    }
  componentWillMount() {
    this.props.getWords();
    this.props.cleanSelectedMoves();
    this.props.cleanSelectedWords();
    this.props.addPlayers(this.props.playerId);
    firebase.database().ref('users/').on('value', (snapshot) => {
      const objLocal = {};
      objLocal.currentUsersValues = snapshot.val();
      if (objLocal.currentUsersValues != null) {
        this.props.getPlayers(objLocal.currentUsersValues);
      }
    });
    firebase.database().ref('selectedMoves/').on('value', (snapshot) => {
      const objLocal = {};
      objLocal.moves = snapshot.val();
      if (objLocal.moves != null) {
        this.props.getSelectedMoves(objLocal.moves);
      }
    });
    firebase.database().ref('selectedWords/').on('value', (snapshot) => {
      const objLocal = {};
      objLocal.currentWords = snapshot.val();
      if (objLocal.currentWords != null) {
        this.props.getSelectedWords(objLocal.currentWords);
      }
    });
  }
  toUpdateData() {
      this.props.getPlayers(this.props.playerId);
      this.props.getSelectedWords();
      this.props.getSelectedMoves();
  }
  getWords() {
    this.props.getWords();
  }

  fillBlanks(puzzle) {
    for (let i = 0; i < puzzle.length; i++) {
      const row = puzzle[i];
      for (let j = 0; j < row.length; j++) {
        if (puzzle[i][j] === '') {
          puzzle[i][j] = this.props.letters[Math.floor(Math.random() * this.props.letters.length)];
        }
      }
    }
  }

      findBestLocations(puzzle, options, word, allMyOrientations) {
          const locations = [];
          const width = options.width;
          const height = options.height;
          const wordLength = word.length;
          let maxOverlap = 0;

          for (let k = 0; k < options.orientations.length; k++) {
            const orientation = options.orientations[k];
            const check = allMyOrientations.checkOrientations[orientation];
            const next = allMyOrientations.orientations[orientation];
            const skipTo = allMyOrientations.skipOrientations[orientation];
            let x = 0;
            let y = 0;
            let overlap;
            while (y < height) {
              if (check(x, y, width, height, wordLength)) {
                overlap = this.checkForOverLap(puzzle, word, x, y, next);
                if (overlap >= maxOverlap || (!options.preferOverlap && overlap > -1)) {
                    maxOverlap = overlap;
                    locations.push({
                       x,
                       y,
                     orientation,
                    overlap
                    });
                  }
                  x++;
                  if (x >= width) {
                    x = 0;
                    y++;
                  }
               } else {
                  const nextPossible = skipTo(x, y, wordLength);
                  x = nextPossible.x;
                  y = nextPossible.y;
                }
              }
            }
            return options.preferOverlap ?
         this.pruneLocations(locations, maxOverlap) :
         locations;
          }

       checkForOverLap(puzzle, word, x, y, fn) {
            let overlap = 0;

            for (let k = 0; k < word.length; k++) {
              const next = fn(x, y, k);
              const score = puzzle[next.x][next.y];
              if (score === word[k]) {
                overlap++;
              } else if (score !== '') {
                return -1;
              }
            }
            return overlap;
          }

      pruneLocations(locations, overlap) {
            const pruned = [];
            for (let i = 0, len = locations.length; i < len; i++) {
              if (locations[i].overlap >= overlap) {
                pruned.push(locations[i]);
              }
            }

            return pruned;
          }


       placeWord(puzzle, word, x, y, fn) {
            for (let i = 0; i < word.length; i++) {
              const next = fn(x, y, i);
              puzzle[next.x][next.y] = word[i];
            }
          }


  placeWordInPuzzle(puzzle, options, word, allMyOrientations) {
          const locations = this.findBestLocations(puzzle, options, word, allMyOrientations);
          if (locations.length === 0) {
            return false;
          }
          const s = locations[Math.floor(Math.random() * locations.length)];
          this.placeWord(puzzle, word, s.x, s.y, allMyOrientations.orientations[s.orientation]);
          return true;
        }

  fillPuzzle(words, options, allMyOrientations) {
          let puzzle = [];
          for (let i = 0; i < options.height; i++) {
            puzzle.push([]);
            for (let j = 0; j < options.width; j++) {
              puzzle[i].push('');
            }
          }
          for (let k = 0; k < words.length; k++) {
            if (!this.placeWordInPuzzle(puzzle, options, words[k], allMyOrientations)) {
              return null;
            }
          }
          return puzzle;
        }

  startTurn(e) {
    const target = e.target;
        this.myobj.startSquare = target;
        this.myobj.selectedSquares.push(target);
        this.myobj.curWord = target.value;
        target.classList.add('selected');
        }
  select(e) {
    const selectTarget = e.target;
    if (!this.myobj.startSquare) {
      return;
    }
    let lastSquare = this.myobj.selectedSquares[this.myobj.selectedSquares.length - 1];
      if (lastSquare === selectTarget) {
        return;
      }

      let backTo;
      for (let i = 0; i < this.myobj.selectedSquares.length; i++) {
        if (selectTarget === this.myobj.selectedSquares[i]) {
          backTo = i + 1;
        }
      }
      while (backTo < this.myobj.selectedSquares.length) {
        this.myobj.selectedSquares[
          this.myobj.selectedSquares.length - 1].classList.remove('selected');
        this.myobj.selectedSquares.splice(backTo, 1);
        this.myobj.curWord = this.myobj.curWord.substr(0, this.myobj.curWord.length - 1);
      }
      const newOrientation = this.calcOrientation(this.myobj.startSquare.dataset.x - 0,
        this.myobj.startSquare.dataset.y - 0,
         selectTarget.dataset.x - 0, selectTarget.dataset.y - 0);
        if (newOrientation) {
          this.myobj.selectedSquares = [this.myobj.startSquare];
          this.myobj.curWord = this.myobj.startSquare.textContent;
          if (lastSquare !== this.myobj.startSquare) {
            lastSquare.classList.remove('selected');
            lastSquare = this.myobj.startSquare;
          }
          this.myobj.curOrientation = newOrientation;
        }

        const orientation = this.calcOrientation(lastSquare.dataset.x - 0,
          lastSquare.dataset.y - 0,
           selectTarget.dataset.x - 0, selectTarget.dataset.y - 0);
        if (!orientation) {
          return;
        }

        if (!this.myobj.curOrientation || this.myobj.curOrientation === orientation) {
          this.myobj.curOrientation = orientation;
          this.playTurn(selectTarget);
        }
}

playTurn(target) {
  for (let i = 0; i < this.props.words.length; i++) {
  if (this.props.words[i].indexOf(this.myobj.curWord + target.textContent) === 0) {
    target.classList.add('selected');
    this.myobj.selectedSquares.push(target);
    this.myobj.curWord = this.myobj.curWord + target.textContent;
    break;
  }
}
}

calcOrientation(x1, y1, x2, y2) {
  for (let i = 0; i < this.props.allMyOrientations.allOrientation.length; i++) {
    const orientation = this.props.allMyOrientations.allOrientation[i];
    const nextFn = this.props.allMyOrientations.orientations[orientation];
    const next = nextFn(x1, y1, 1);
    if (next.x === x2 && next.y === y2) {
      return orientation;
    }
  }
  return null;
}

  endTurn() {
    const allSelected = document.getElementsByClassName('selected');
        for (let i = 0, len = this.props.wordList.length; i < len; i++) {
          if (this.props.wordList[i] === this.myobj.curWord) {
            this.props.addPoints(this.props.currentUser, this.props.currentUserId);

            this.props.wordSelected(this.myobj.curWord,
               this.props.currentUser, this.props.allSelectedWords.length);

            this.props.movesSelected(this.myobj.selectedSquares,
               this.props.currentUser, this.props.allSelectedMoves.length, this.myobj.curWord);
            break;
          }
        }
        if (this.props.wordList.length === 0) {
          document.querySelector('.game-box').classList.add('complete');
          this.props.endgame();
        }
        while (allSelected.length > 0) {
          allSelected[0].classList.remove('selected');
        }
        this.myobj.startSquare = null;
        this.myobj.selectedSquares = [];
        this.myobj.curWord = '';
        this.myobj.curOrientation = null;
        }

        printPuzzle() {
          if (this.props.puzzle === null) {
            return;
          }
          const html = this.props.puzzle.map((d, i) => {
              const myhtml = d.map((a, j) => {
                return (
                  <button
                  className="each-letter" data-x={j} data-y={i} key={i + j}
                  onMouseDown={(e) => this.startTurn(e)}
                  onMouseEnter={(e) => this.select(e)}
                  >{a}
                  </button>
                );
              });
              return (<div key={i}>{myhtml}</div>);
            });
            return html;
        }
        getPlayers() {
          return this.props.users
           ?
          <div>
          <div className="first-player">
            <div>{this.props.users[0].email}</div>
            <div>{this.props.users[0].points}</div>
          </div>
          <div className="second-player">
          <div>{this.props.users[1].email}</div>
          <div>{this.props.users[1].points}</div>
          </div>
          </div>
           : null;
          }

        callFillPuzzle() {
          firebase.database().ref('puzzle').once('value', (snapshot) => {
            const p = snapshot.val();
            if (p === null) {
              let puzzle;
              let attempts = 0;
              const wordList = this.props.words.slice(0).sort((a, b) =>
              ((b.length - a.length > 0) ? 1 : -1) || b.localCompare(a));
              const opts = this.props.settings || {};
              // this.props.createOptions(opts, wordList);
              const options = {};
              options.width = opts.width || wordList[0].length;
              options.height = opts.height || wordList[0].length;
              options.orientations = opts.orientations ||
                                      this.props.allMyOrientations.allOrientation;
              options.preferOverlap = opts.preferOverlap === undefined ? true : opts.preferOverlap;
              options.fillBlanks = opts.fillBlanks === undefined ? true : opts.fillBlanks;
              options.maxattempts = opts.maxattempts || 3;
              while (!puzzle) {
                        while (!puzzle && attempts++ < options.maxattempts) {
                          puzzle = this.fillPuzzle(wordList, options, this.props.allMyOrientations);
                        }

                        if (!puzzle) {
                          options.width++;
                          options.height++;
                          attempts = 0;
                        }
                      }
                      if (options.fillBlanks) {
                        this.fillBlanks(puzzle);
                      }
                    this.props.fillPuzzle(puzzle);
            } else {
              this.props.fillPuzzle(p);
            }
          });
        }
          checkIfPrint() {
            if (this.props.puzzle !== null) {
            return this.printPuzzle(this.props.puzzle);
            }
            return <button onClick={(e) => this.callFillPuzzle(e)}>print Puzzle</button>;
          }
          scoreOfAll() {
            console.log('ok');
          }
        changeInView() {
          const wordToDelete = this.props.allSelectedWords;
          const movesToColor = this.props.allSelectedMoves;
          if (wordToDelete.length > 0) {
            for (let i = 0; i < wordToDelete.length; i++) {
              document.querySelector(`div[data-key="${wordToDelete[i].word}"]`)
              .classList.add('wordFound');
            }
          }
          if (movesToColor.length > 0) {
            for (let j = 0; j < movesToColor.length; j++) {
              for (let k = movesToColor[j].word.length - 1; k >= 0; k--) {
                const p = movesToColor[j].move[k];
                const c = document.querySelectorAll(`button[data-x="${p.x}"]`);
                let a;
                for (let m = 0; m < c.length; m++) {
                  if (c[m].dataset.y === p.y) {
                   a = c[m];
                  }
                }
                a.classList.add('found');
              }
            }
          }
          // for(let i = 0; i < this.props.allSelectedWords.length; i++) {}
        }
        GameOrScore() {
          if (this.props.words !== null) {
            if (this.props.wordList.length === 0) {
              this.props.cleanPuzzle();

              return <div className="score-card">{this.scoreOfAll()}</div>;
            }
            return (
              <div
              className="word-box" onMouseUp={(e) => this.endTurn(e)}
              >
              selectedMoves are {this.props.allSelectedMoves.length}
              <div>
                Welcome <h2>{this.props.currentUser ? this.props.currentUser.name : ''}</h2>
              </div>
              <div className="two-player">
              {this.getPlayers()}
              </div>
              <div className="game-box">
              {this.checkIfPrint()}
              </div>
              <Words />
              </div>
            );
          }
        }
        getUser() {
          const user = firebase.auth().currentUser;
          let name = 'no name';
          if (user != null) {
            name = user.displayName;
          }
          return name;
        }
  render() {
    this.changeInView();
    return (
    <div>
    {this.getUser()}
    { this.GameOrScore()}

    </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    words: state.PuzzleData.words,
    wordList: state.PuzzleData.wordList,
    settings: state.PuzzleData.settings,
    opts: state.PuzzleData.opts,
    allMyOrientations: state.PuzzleData,
    letters: state.PuzzleData.letters,
    puzzle: state.PuzzleData.puzzle,
    users: state.PuzzleData.users,
    currentUser: state.PuzzleData.currentUser,
    currentUserId: state.PuzzleData.currentUserId,
    allSelectedWords: state.PuzzleData.allSelectedWords,
    allSelectedMoves: state.PuzzleData.allSelectedMoves
  };
}

export default connect(mapStateToProps,
   {
     createOptions,
     fillPuzzle,
     getPlayers,
     addPlayers,
      changeCurrentUser,
      addPoints,
      wordSelected,
      getWords,
    getSelectedWords,
  cleanSelectedWords,
   cleanSelectedMoves,
   getSelectedMoves,
    movesSelected,
  endgame,
cleanPuzzle })(NewPuzzle);
