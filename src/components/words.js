import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getWords } from '../actions';

class Words extends Component {
  componentWillMount() {
    this.props.getWords();
  }

  writeAllWord() {
  if (this.props.words !== null) {
    const html = this.props.words.map((d, i) => {
      return <div className="each-word" key={i} data-key={d}>{d}</div>;
    });
    return html;
  }
  return null;
  }
  render() {
    return (
      <div className="given-word">
      {this.writeAllWord()}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    words: state.PuzzleData.words
  };
}

export default connect(mapStateToProps, { getWords })(Words);
