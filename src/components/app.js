import React, { Component } from 'react';
import { connect } from 'react-redux';
import NewPuzzle from './newPuzzle.js';
import { addUser, removeUser } from '../actions';

class App extends Component {
  constructor(props) {
    super(props);
    this.loginWithFacebook = this.loginWithFacebook.bind(this);
    this.logOut = this.logOut.bind(this);
    this.getUser = this.getUser.bind(this);
  }

  loginWithFacebook() {
    firebase.auth().signInWithPopup(provider).then((result) => {
      const token = result.credential.accessToken;
      const user = result.user;
      const objLocal = {};
      objLocal.token = token;
      objLocal.user = user;
      console.log('User is ', user.displayName);
      this.props.addUser(user.displayName);
    });
  }
  getUser() {
    const user = firebase.auth().currentUser;
    let name = 'no name';
    if (user != null) {
      name = user.displayName;
    }
    return name;
  }
  /*
  https://wordpuzzle-74d40.firebaseapp.com/__/auth/handler
  https://wordpuzzle-74d40.firebaseapp.com/__/auth/handler
  */
  logOut() {
    firebase.auth().signOut().then(() => {
      this.props.removeUser();
    });
  }
  render() {
    return (
      <div>
      <div>
      <div>{this.getUser()}</div>
      <button onClick={this.loginWithFacebook.bind(this)}>Login with Facebook</button>
      <button onClick={this.logOut.bind(this)}>Log Out</button>
      </div>

      <NewPuzzle playerId={this.props.match.params.id - 1} />

      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
  users: state.PuzzleData.users
  };
}
export default connect(mapStateToProps, { addUser, removeUser })(App);
