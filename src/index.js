import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import promise from 'redux-promise';
import thunk from 'redux-thunk';
import logger from 'redux-logger';

import App from './components/app';
import reducers from './reducers';

const createStoreWithMiddleware = applyMiddleware(promise, thunk, logger)(createStore);
ReactDOM.render(
  <Provider store={createStoreWithMiddleware(reducers)}>
  <Router>
      <Route path="/users/:id" component={App} />
  </Router>
  </Provider>
  , document.querySelector('.container'));
