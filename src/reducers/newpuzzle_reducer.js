import { CREATEOPTIONS, FILLPUZZLE, CLEANSELECTMOVES,
   CLEANSELECTWORDS, GETALLSELECTEDMOVES, GETALLSELECTEDWORDS,
   CLEANPUZZLE } from '../actions/index.js';

const INITIAL_STATE = {
  letters: 'abcdefghijklmnopqrstuvwxyz',
  allOrientation: ['horizontal', 'horizontalBack', 'vertical',
  'verticalUp', 'diagonal', 'diagonalUp', 'diagonalBack', 'diagonalUpBack'],
  orientations: {
        horizontal: (x, y, l) => {
          return {
            x: x + l,
            y
          };
        },
        horizontalBack: (x, y, l) => {
          return {
            x: x - l,
            y
          };
        },
        vertical: (x, y, l) => {
          return {
            x,
            y: y + l
          };
        },
        verticalUp: (x, y, l) => {
          return {
            x,
            y: y - l
          };
        },
        diagonal: (x, y, l) => {
          return {
            x: x + l,
            y: y + l
          };
        },
        diagonalUp: (x, y, l) => {
          return {
            x: x + l,
            y: y - l
          };
        },
        diagonalBack: (x, y, l) => {
          return {
            x: x - l,
            y: y + l
          };
        },
        diagonalUpBack: (x, y, l) => {
          return {
            x: x - l,
            y: y - l
          };
        }
      },
  checkOrientations: {
        horizontal: (x, y, w, h, l) => {
          return w >= x + l;
        },
        horizontalBack: (x, y, w, h, l) => {
          return x + 1 >= l;
        },
        vertical: (x, y, w, h, l) => {
          return h >= y + l;
        },
        verticalUp: (x, y, w, h, l) => {
          return y + 1 >= l;
        },
        diagonal: (x, y, w, h, l) => {
          return (w >= x + l && h >= y + l);
        },
        diagonalUp: (x, y, w, h, l) => {
          return (w >= x + l && y + 1 >= l);
        },
        diagonalBack: (x, y, w, h, l) => {
          return (x + 1 >= l && h >= y + l);
        },
        diagonalUpBack: (x, y, w, h, l) => {
          return (x + 1 >= l && y + 1 >= l);
        }
      },
  skipOrientations: {
        horizontal: (x, y, l) => {
          return {
            x: 0,
            y: y + 1
          };
        },
        horizontalBack: (x, y, l) => {
          return {
            x: l - 1,
            y
          };
        },
        vertical: (x, y, l) => {
          return {
            x,
            y: y + 100
          };
        },
        verticalUp: (x, y, l) => {
          return {
            x: 0,
            y: l - 1
          };
        },
        diagonal: (x, y, l) => {
          return {
            x: 0,
            y: y + 1
          };
        },
        diagonalUp: (x, y, l) => {
          return {
            x: 0,
            y: y < l - 1 ? l - 1 : y + 1
          };
        },
        diagonalBack: (x, y, l) => {
          return {
            x: l - 1,
            y: x < l - 1 ? y : y + 100
          };
        },
        diagonalUpBack: (x, y, l) => {
          return {
            x: l - 1,
            y: x >= l - 1 ? l - 1 : y
          };
        }
      },
  words: null,
  wordList: null,
  settings: null,
  allSelectedWords: [],
  allSelectedMoves: [],
  puzzle: null,
  opts: null,
  currentUserId: null,
  fillBlanks: null,
  users: null,
  currentUser: null,
  currentPlayer: null
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case 'addUser': {
      return { ...state, currentPlayer: action.payload };
    }
    case 'removeUser': {
      return { ...state, currentPlayer: action.payload };
    }
    case GETALLSELECTEDWORDS: {
      const words = action.payload === null ? [] : action.payload;
      let updatedWordList;
      if (words.length !== 0) {
        updatedWordList = state.wordList.filter((d) => {
          for (let i = 0; i < words.length; i++) {
            if (words[i].word === d) {
              return false;
            }
          }
          return true;
        });
      } else {
        updatedWordList = state.wordList;
      }
      return { ...state, allSelectedWords: words, wordList: updatedWordList };
    }
    case CLEANPUZZLE: {
      return { ...state, puzzle: null };
    }
    case GETALLSELECTEDMOVES: {
      return { ...state, allSelectedMoves: action.payload === null ? [] : action.payload };
    }
    case CLEANSELECTMOVES: {
      return { ...state, allSelectedMoves: [] };
    }
    case CLEANSELECTWORDS: {
      return { ...state, allSelectedWords: [] };
    }
    case CREATEOPTIONS: {
      return { ...state, opts: action.payload };
    }
    case 'GETWORDS': {
      const wordlist = action.payload.slice(0).sort((a, b) =>
        ((b.length - a.length > 0) ? 1 : -1) || b.localCompare(a)
      );
      return { ...state, words: action.payload, wordList: wordlist };
    }
    case FILLPUZZLE: {
      return { ...state, puzzle: action.payload };
    }
    case 'ADDPLAYERS': {
      const user = state.users[action.payload];
      return { ...state, currentUserId: action.payload, currentUser: user };
    }
    case 'GETPLAYERS': {
      return { ...state,
         users: action.payload.players };
    }
    case 'addPoints': {
      const user = JSON.parse(JSON.stringify(state.currentUser));
      user.points += 3;
      return { ...state, currentUser: user };
    }
    default:
    return state;
  }
}
