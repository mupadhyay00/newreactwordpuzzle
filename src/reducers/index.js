import { combineReducers } from 'redux';
import newPuzzleReducer from './newpuzzle_reducer.js';

const rootReducer = combineReducers({
  PuzzleData: newPuzzleReducer
});

export default rootReducer;
